# Todolist

## Infos

- Pour les vues Bootstrap 4 a été utilisé.
- Pour les icons Ion Icon a été utilisé.
- On peut ajouter, soit une categorie ou un film, le supprimer et l'editer selon le cas.
- Les films ont un titre, un acteur, une duree et une categorie.
- Les categories ont un type et une description.
- On marquer les films comme 'regardés' et les modifier (les boutons à droite du tableau).
- Les films et les genres peuvent être exportés en fichiers CSV.
- Une authentification par mail ou google est disponible grâce à l'implementation d'Auth0.
- Un bouton est disponible pour supprimer toutes les films et un autre pour les genres (avec une confirmation avant et, aussi, tant que l'user ayant été authentifié).

## Installation

```shell
npm install express
```

## Lancement

Dans un autre terminal, lancez mongo:

```shell
mongo
```

Dans le dossier de votre app, lancez-là:

```shell
node server.js
```

## Paquets utilisés

### Node

- body-parser
- express
- express-flash
- express-session
- csv-express
- fast-csv
- mongoose
- nunjucks
- bulma
- express-openid-connect

### Front

- Bootstrap 4
- sorttablejs
- Bulma
