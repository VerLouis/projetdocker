// require('dotenv').config({ path: './env' });
const path = require('path') // gestion fichiers locaux
const express = require('express') //framework mvc
const nunjucks = require('nunjucks') // templates
const session = require('express-session') // sessions
const bodyParser = require('body-parser')
const mongoose = require('mongoose');
// const { auth } = require('express-openid-connect'); // Authentication avec Auth0
const csv = require('csv-express'); // CSV-express pour l'exportation des données

// Ajoute la configuration du fichier config par path.join
const config = require(path.join(__dirname, 'config.js'))

/*
const config2 = {
    authRequired: false,
    auth0Logout: true,
    secret: process.env.SECRET,
    baseURL: process.env.BASEURL,
    clientID: process.env.CLIENTID,
    issuerBaseURL: process.env.ISSUER
};
*/

// Création du Schéma Film MongoDB
const TodoFilmSchema = new mongoose.Schema({
    titre: { type: String, required: true },
    auteur: { type: String },
    duree: { type: String },
    categorie: { type: String },
})

// Création du Schéma GenreFilm MongoDB
const TodoGenreSchema = new mongoose.Schema({
    type: { type: String, required: true },
    description: { type: String },
})

// to fix all deprecation warnings
mongoose.set('useFindAndModify', false);
mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);
mongoose.set('useCreateIndex', true);

const TodoFilm = mongoose.model('Film', TodoFilmSchema)
const TodoGenre = mongoose.model('Genre', TodoGenreSchema)

// Connection avec mongodb
mongoose.connect('mongodb://mongodatabase/todos_mongo')
mongoose.connection.on('error', err => {
    console.error(err)
})

let app = express()


nunjucks.configure('views', {
    express: app
})

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

let sessionStore = new session.MemoryStore()

app.use(express.static(path.join(__dirname, '/views')))
app.use(session({
    cookie: { maxAge: 60000 },
    store: sessionStore,
    saveUninitialized: true,
    resave: 'true',
    secret: config.express.cookieSecret
}))

app.use((req, res, next) => {
    next()
})

let router = express.Router()

router.route('/')
    .get((req, res) => {
        TodoFilm.find().then(todos => {
            if (typeof message !== "undefined") {
                res.render('todo.njk', { todos: todos, message: message })
            } else {
                res.render('todo.njk', { todos: todos })
            }
        }).catch(err => {
            console.error(err)
        })
    })

router.route('/genre')
    .get((req, res) => {
        TodoGenre.find().then(todosG => {
            if (typeof messageGenre !== "undefined") {
                res.render('todoG.njk', { todosG: todosG, messageGenre: messageGenre })
            } else {
                res.render('todoG.njk', { todosG: todosG })
            }
        }).catch(err => {
            console.error(err)
        })
    })

router.route('/add')
    .post((req, res) => {
        new TodoFilm({
            titre: req.body.inputTitre,
            auteur: req.body.inputAuteur,
            duree: req.body.inputDuree,
            categorie: req.body.inputCategorie
        }).save().then(todo => {
            console.log('Votre film a été ajouté');
            message = "Votre film a été ajouté";
            res.redirect('/todo' + message)
        }).catch(err => {
            console.warn(err);
        })
    })

router.route('/addGenre')
    .post((req, res) => {
        new TodoGenre({
            type: req.body.inputType,
            description: req.body.inputDescription,
        }).save().then(todo => {
            console.log('Votre genre a été ajouté');
            messageGenre = "Votre genre a été ajouté";
            res.redirect('/todo/genre' + messageGenre)
        }).catch(err => {
            console.warn(err);
        })
    })

router.route('/edit/:id')
    .get((req, res) => {
        TodoFilm.findById(req.params.id).then(todo => {
            res.render('edit.njk', { todo: todo })
        }).catch(err => {
            console.error(err)
        })
    })
    .post((req, res) => {
        TodoFilm.updateOne({
            _id: req.params.id
        }, {
            $set: {
                titre: req.body.inputTitre,
                auteur: req.body.inputAuteur,
                duree: req.body.inputDuree,
                categorie: req.body.inputCategorie,
            }
        }).then(() => {
            console.log("Film modifié")
            message = "Film modifié";
            res.redirect('/todo' + message)
        }).catch(err => {
            console.error(err)
        })
    })

router.route('/editG/:id')
    .get((req, res) => {
        TodoGenre.findById(req.params.id).then(todo => {
            res.render('editG.njk', { todo: todo })
        }).catch(err => {
            console.error(err)
        })
    })
    .post((req, res) => {
        TodoGenre.updateOne({
            _id: req.params.id
        }, {
            $set: {
                type: req.body.inputType,
                description: req.body.inputDescription,
            }
        }).then(() => {
            console.log('Genre modifié')
            messageGenre = "Genre modifié";
            res.redirect('/todo/genre', messageGenre)
        }).catch(err => {
            console.error(err)
        })
    })

router.route('/delete/all')
    .get((req, res) => {
        // if (req.oidc.isAuthenticated()) {
        TodoFilm.deleteMany().then(() => {
                console.log('Tous les films sont enlevés');
                message = "Tous les films sont enlevés";
                res.redirect('/todo' + message);
            }).catch(err => {
                console.error(err)
            })
            //  } else {
        console.log("L'user n'est pas authentifié");
        res.redirect('/todo');
        //}
    })

router.route('/delete/allgenres')
    .get((req, res) => {
        // if (req.oidc.isAuthenticated()) {
        TodoGenre.deleteMany().then(() => {
                console.log('Tous les genres sont enlevés');
                messageGenre = "Tous les genres sont enlevés";
                res.redirect('/todo/genre' + messageGenre)
            }).catch(err => {
                console.error(err)
            })
            //  } else {
        console.log("L'user n'est pas authentifié");
        res.redirect('/todo/genre');
        // }
    })

router.route('/delete/:id')
    .get((req, res) => {
        TodoFilm.findByIdAndRemove({ _id: req.params.id }).then(() => {
            console.log('Votre film est enlevé');
            message = "Votre film est enlevé";
            res.redirect('/todo' + message)
        }).catch(err => {
            console.error(err)
        })
    })

router.route('/deleteG/:id')
    .get((req, res) => {
        TodoGenre.findByIdAndRemove({ _id: req.params.id }).then(() => {
            console.log('Votre genre est enlevé');
            messageGenre = "Votre genre est enlevé";
            res.redirect('/todo/genre' + messageGenre)
        }).catch(err => {
            console.error(err)
        })
    })

router.get('/exporttocsv', function(req, res, next) {
    let filename = "films.csv";
    let dataArray;

    TodoFilm.find().lean().exec({}, function(err, films) {
        if (err) {
            res.send(err);
        }
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/csv');
        res.setHeader("Content-Disposition", 'attachment; filename=' + filename);
        res.csv(films, true);
        console.log('Films exportés');
    })
})

router.get('/exporttocsvGenres', function(req, res, next) {
    let filename = "genres.csv";
    let dataArray;

    TodoGenre.find().lean().exec({}, function(err, genres) {
        if (err) {
            res.send(err);
        }
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/csv');
        res.setHeader("Content-Disposition", 'attachment; filename=' + filename);
        res.csv(genres, true);
        console.log('Genres exportés');
    })
})


app.use('/todo', router)
app.use('/pub', express.static('public'))
app.use((req, res) => {
    res.redirect('/todo')
})

app.listen(config.express.port, config.express.ip, () => {
    console.log('Server listening on ' + config.express.ip + ':' + config.express.port)
})